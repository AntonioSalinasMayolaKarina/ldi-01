﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author kary
 */
public class Sumas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
       int op;
       
       Scanner leer = new Scanner(System.in);
       
       do{
       System.out.println("--- OPERACIONES ---");
       System.out.println("1.- Suma de Bytes");
       System.out.println("2.- Suma de Word");
       System.out.println("3.- Suma de Double Word");
       System.out.println("4.- Suma de Quad Word");
       System.out.println("5.- Salir");
       op=leer.nextInt();
       
       switch(op){
       
    case 1:
      
       String op1;
       do{
       System.out.println("--- OPERACIONES CON BYTE --- ");
       System.out.println("a)Suma: BYTE + BYTE");
       System.out.println("b)Suma: BYTE + WORD");
       System.out.println("c)Suma: BYTE + DWORD");
       System.out.println("d)Suma: BYTE + QWORD");
       System.out.println("r) Regresar al menu principal");
       op1=leer.next();
       
       switch(op1){
           case "a":
                String byte1;
                String byte2;
                 //BYTE variable de 8 bits = Tipo de dato BYTE en java
                System.out.println("Ingresa el primer numero en byte a sumar:");
                byte1=leer.next();
                System.out.println("Ingresa el segundo numero en byte a sumar:");
                byte2=leer.next();
        
                if (byte1.length() == 8 && byte2.length() == 8)
                {
             
                byte numero1 = Byte.parseByte(byte1, 2);
                byte numero2 = Byte.parseByte(byte2, 2);
                byte suma = (byte) (numero1 + numero2);
      
                String resultado = Integer.toString(suma, 2);
                int longnum = resultado.length();
                int numcaracter = 8 - longnum;
                
                System.out.println("El resultado de la suma de byte es: ");
        
                for (int i=0; i<numcaracter; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(resultado);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 8 bytes de tamaño, verifica tus datos");
                }
                break;
               
               case "b":
                String bytesum1;
                String wordsum1;
               
                System.out.println("Ingresa el primer numero en byte a sumar:");
                bytesum1=leer.next();
                System.out.println("Ingresa el segundo numero en word a sumar:");
                wordsum1=leer.next();
        
                if (bytesum1.length() == 8 && wordsum1.length() == 16)
                {
             
                byte numero1 = Byte.parseByte(bytesum1, 2);
                long numero2 = Long.parseLong(wordsum1, 2);
                long suma = (long) (numero1 + numero2);
      
                String resultado = Long.toString(suma, 2);
                int longnum = resultado.length();
                int numcaracter = 16 - longnum;
                
                System.out.println("El resultado de la suma entre un byte y un word es: ");
                for (int i=0; i<numcaracter; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(resultado);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
                }
                break;
                   
               case "c":
                String bytesum2;
                String wordsum2;
               
                System.out.println("Ingresa el primer numero en byte a sumar:");
                bytesum2=leer.next();
                System.out.println("Ingresa el segundo numero en double word a sumar:");
                wordsum2=leer.next();
       
                if (bytesum2.length() == 8 && wordsum2.length() == 32)
                {
             
                byte numero1 = Byte.parseByte(bytesum2, 2);
                long numero2 = Long.parseLong(wordsum2, 2);
                long suma = (long) (numero1 + numero2);
      
                String resultado = Long.toString(suma, 2);
                int longnum = resultado.length();
                int numcaracter = 32 - longnum;
                
                System.out.println("El resultado de la suma entre un byte y un double word es: ");
        
                for (int i=0; i<numcaracter; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(resultado);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
                }
                break;
                   
            case "d":
                String bytesum3;
                String wordsum3;
               
                System.out.println("Ingresa el primer numero en byte a sumar:");
                bytesum3=leer.next();
                System.out.println("Ingresa el segundo numero en quad word a sumar:");
                wordsum3=leer.next();
       
                if (bytesum3.length() == 8 && wordsum3.length() == 64)
                {
             
                byte numero1 = Byte.parseByte(bytesum3, 2);
                long numero2 = Long.parseLong(wordsum3, 2);
                long suma = (long) (numero1 + numero2);
      
                String resultado = Long.toString(suma, 2);
                int longnum = resultado.length();
                int numcaracter = 64 - longnum;
                
                System.out.println("El resultado de la suma entre un byte y un quad word es: ");
        
                for (int i=0; i<numcaracter; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(resultado);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
                }
                break;
       }
       }while(!"r".equals(op1));
    break;
    
    case 2:
       String op2;
       do{
       System.out.println("--- OPERACIONES CON WORD ---");
       System.out.println("a)Suma: WORD + WORD");
       System.out.println("b)Suma: WORD + BYTE");
       System.out.println("c)Suma: WORD + DWORD");
       System.out.println("d)Suma: WORD + QWORD");
       System.out.println("r) Regresar al menu principal");
       op2=leer.next();
       
       switch(op2){
        
        case "a":
        String word1;
        String word2;
       
        System.out.println("Ingresa el primer valor word para sumar:");
        word1=leer.next();
        System.out.println("Ingresa la segunda word para sumar:");
        word2=leer.next();
        
        if (word1.length() == 16 && word2.length() == 16){
             
        long numero1 = Short.parseShort(word1,2);
        long numero2 = Short.parseShort(word2,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 16 - longnum;
        System.out.println("El resultado de la suma de word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
         
        case "b":
        String wordsum1;
        String bytesum1;
      
        System.out.println("Ingresa el primer valor word para sumar:");
        wordsum1=leer.next();
        System.out.println("Ingresa el segundo valor byte para sumar:");
        bytesum1=leer.next();
        
        if (wordsum1.length() == 16 && bytesum1.length() == 8){
             
        long numero1 = Short.parseShort(wordsum1,2);
        long numero2 = Short.parseShort(bytesum1,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 16 - longnum;
        System.out.println("El resultado de la suma de un word y un byte es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "c":
        String wordsum2;
        String bytesum2;
       
        System.out.println("Ingresa el primer valor word para sumar:");
        wordsum2=leer.next();
        System.out.println("Ingresa el segundo valor double word para sumar:");
        bytesum2=leer.next();
        
        if (wordsum2.length() == 16 && bytesum2.length() == 32){
             
        long numero1 = Short.parseShort(wordsum2,2);
        long numero2 = Short.parseShort(bytesum2,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 32 - longnum;
        System.out.println("El resultado de la suma de un word y un double word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "d":
        String wordsum3;
        String bytesum3;
  
        System.out.println("Ingresa el primer valor word para sumar:");
        wordsum3=leer.next();
        System.out.println("Ingresa el segundo valor quad word para sumar:");
        bytesum3=leer.next();
        
        if (wordsum3.length() == 16 && bytesum3.length() == 64){
             
        long numero1 = Short.parseShort(wordsum3,2);
        long numero2 = Short.parseShort(bytesum3,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 64 - longnum;
        System.out.println("El resultado de la suma de un word y un quad word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
               
       }
       }while(!"r".equals(op2));
       break;
     
        
    case 3:
        String op3;
       do{
       System.out.println("--- OPERACIONES CON DWORD ---");
       System.out.println("a)Suma: DWORD + DWORD");
       System.out.println("b)Suma: DWORD + BYTE");
       System.out.println("c)Suma: DWORD + WORD");
       System.out.println("d)Suma: DWORD + QWORD");
       System.out.println("r) Regresar al menu principal");
       op3=leer.next();
       
       switch(op3){
        
        case "a": 
        String dword1;
        String dword2;
    
        System.out.println("Ingresa el primer double word para sumar:");
        dword1=leer.next();
        System.out.println("Ingresa el segundo double word para sumar:");
        dword2=leer.next();
       
        if (dword1.length() == 32 && dword2.length() == 32){
             
        long numero1 = Long.parseLong(dword1,2);
        long numero2 = Long.parseLong(dword2,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 32 - longnum;
        System.out.println("El resultado de la suma de double word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
        }
        break;
            
        case "b": 
        String dwordsum1;
        String bytesum1;
    
        System.out.println("Ingresa el primer valor double word para sumar:");
        dwordsum1=leer.next();
        System.out.println("Ingresa el segundo valor byte para sumar:");
        bytesum1=leer.next();
       
        if (dwordsum1.length() == 32 && bytesum1.length() == 8){
             
        long numero1 = Long.parseLong(dwordsum1,2);
        long numero2 = Long.parseLong(bytesum1,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 32 - longnum;
        System.out.println("El resultado de la suma de double word y byte es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "c": 
        String dwordsum2;
        String bytesum2;
    
        System.out.println("Ingresa el primer valor double word para sumar:");
        dwordsum2=leer.next();
        System.out.println("Ingresa el segundo valor word para sumar:");
        bytesum2=leer.next();
       
        if (dwordsum2.length() == 32 && bytesum2.length() == 16){
             
        long numero1 = Long.parseLong(dwordsum2,2);
        long numero2 = Long.parseLong(bytesum2,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 32 - longnum;
        System.out.println("El resultado de la suma de double word y word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
        }
        break;
            
         case "d": 
        String dwordsum3;
        String bytesum3;
    
        System.out.println("Ingresa el primer valor double word para sumar:");
        dwordsum3=leer.next();
        System.out.println("Ingresa el segundo valor quad word para sumar:");
        bytesum3=leer.next();
       
        if (dwordsum3.length() == 32 && bytesum3.length() == 64){
             
        long numero1 = Long.parseLong(dwordsum3,2);
        long numero2 = Long.parseLong(bytesum3,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 64 - longnum;
        System.out.println("El resultado de la suma de double word y quad word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;
             
       }
     }while(!"r".equals(op3));
       break;
        
    case 4:
        
       String op4;
       do{
       System.out.println("--- OPERACIONES CON QWORD ---");
       System.out.println("a)Suma: QWORD + QWORD");
       System.out.println("b)Suma: QWORD + BYTE");
       System.out.println("c)Suma: QWORD + WORD");
       System.out.println("d)Suma: QWORD + DWORD");
       System.out.println("r) Regresar al menu principal");
       op4=leer.next();
       
       switch(op4){
        
       case "a":
       String qword1;
       String qword2;
        //QUAD WORD variable de 64 bits = Tipo de dato LONG en java
        System.out.println("Ingresa el primer quad word para sumar:");
        qword1=leer.next();
        System.out.println("Ingresa el segundo quad word para sumar:");
        qword2=leer.next();
       
        if (qword1.length() == 64 && qword2.length() == 64){
             
        long numero1 = Short.parseShort(qword1,2);
        long numero2 = Short.parseShort(qword2,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 64 - longnum;
        System.out.println("El resultado de la suma de quad word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "b":
       String qwordsum1;
       String bytesum1;
        
        System.out.println("Ingresa el primer valor quad word para sumar:");
        qwordsum1=leer.next();
        System.out.println("Ingresa el segundo valor byte para sumar:");
        bytesum1=leer.next();
       
        if (qwordsum1.length() == 64 && bytesum1.length() == 8){
             
        long numero1 = Long.parseLong(qwordsum1,2);
        long numero2 = Long.parseLong(bytesum1,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 64 - longnum;
        System.out.println("El resultado de la suma entre quad word y byte es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;
            
       case "c":
       String qwordsum2;
       String bytesum2;
        
        System.out.println("Ingresa el primer valor quad word para sumar:");
        qwordsum2=leer.next();
        System.out.println("Ingresa el segundo valor word para sumar:");
        bytesum2=leer.next();
       
        if (qwordsum2.length() == 64 && bytesum2.length() == 16){
             
        long numero1 = Long.parseLong(qwordsum2,2);
        long numero2 = Long.parseLong(bytesum2,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 64 - longnum;
        System.out.println("El resultado de la suma entre quad word y word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;  
           
            case "d":
       String qwordsum3;
       String bytesum3;
        
        System.out.println("Ingresa el primer valor quad word para sumar:");
        qwordsum3=leer.next();
        System.out.println("Ingresa el segundo valor double word para sumar:");
        bytesum3=leer.next();
       
        if (qwordsum3.length() == 64 && bytesum3.length() == 32){
             
        long numero1 = Long.parseLong(qwordsum3,2);
        long numero2 = Long.parseLong(bytesum3,2);
        long suma = (long) (numero1 + numero2);
      
        String resultado = Long.toString(suma, 2);
        int longnum = resultado.length();
        int numcaracter = 64 - longnum;
        System.out.println("El resultado de la suma entre quad word y double word es: ");
        
        for (int i=0; i<numcaracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;    
       }
       }while(!"r".equals(op4));
        break;
    }
}while(op != 5);
    }
}